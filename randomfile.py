#!/usr/bin/env python3

import os
import random
import sendmail
from tika import parser

def getRandomFile(path, files, files_count):
  index = random.randrange(0, files_count)
  return files[index]

def publishfile(file):

    parsed_tika=parser.from_file(file)
    text = parsed_tika["content"]
    if text == None:
        print('no text, publishing attachment')
        sender.send_file(file) 
        print('publishing: ', file)
    else:
        sender.send_text(text) 
        print(text)
    return True

def alreadypublished(file):
    
    with open('published.log', 'r') as f:
            for line in f:
                if file in line:
                    print('file in line')
                    return True
                else:
                    continue
    return False

if __name__ == '__main__':

    sender = sendmail.mailsender()

    files = os.listdir('randomfiles')
    files_count = len(files) 
    published = False 

    count = 0
    while published == False:
        count += 1
        if count > files_count:
            print('empty file to start over')
            open('published.log', 'w').close()
            break
        file = getRandomFile('randomfiles', files, files_count)
        #print(file)
        if alreadypublished(file):
            print('next one')
            continue
        else: 
            print('new', file)
            if publishfile('randomfiles/' + file):
                published = True
                break

    if published == True: 
        with open('published.log', 'a') as f:
            print(file, file=f)
