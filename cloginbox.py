#!/usr/bin/env python3
#import eventlet
#imapclient = eventlet.import_patched('imapclient')

import mailparser
from datetime import datetime, time
import pprint
pp = pprint.PrettyPrinter(depth=6)
import datetime
import uuid
import logging
import sys
import traceback
import logging
from logging.handlers import RotatingFileHandler
#

pp = pprint.PrettyPrinter(depth=6)
# Setup the log handlers to stdout and file.
log = logging.getLogger('web_publisher')
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s | %(name)s | %(levelname)s | %(message)s'
    )
handler_stdout = logging.StreamHandler(sys.stdout)
handler_stdout.setLevel(logging.DEBUG)
handler_stdout.setFormatter(formatter)
log.addHandler(handler_stdout)

def publish(msg):
    outputto = open("/var/www/clog/snippets.txt", "a")
    def add_tags(tag, clas, elem, close):
        return "<%s %s>%s</%s>" % (tag, clas, elem, tag)

    outputto.write("\n")
    outputto.write('<div class="post">')
    outputto.write("\n")
    outputto.write(add_tags('div', 'class="subject"',  msg['subject'], '/div'))
    outputto.write("\n")
    outputto.write(add_tags('div', 'class="date"',  msg['date'], '/div'))
    outputto.write("\n")
    outputto.write(add_tags('div', 'class="text"',  msg['body'], '/div'))
    outputto.write("\n")


    log.info('publishing message: ' + str(msg['date']) + ' ' + str(msg['subject']))

    for a in msg['attachments']:
        #print(a['mimetype'] + ' ' + a['description'])
        outputto.write('<div class="imgbox">')
        outputto.write("\n")
        filename = str(uuid.uuid4()) + '.' + a['extension']
        try:
            f = open("/var/www/clog/pics/" + filename, 'wb')

        except:
            log.critical('fopen failed!', exc_info=True)

            return False

        try:
            f.write(a['data'])
            file= "pics/"+filename
            outputto.write("<img src=" + '"%s"' %  file + ">")
            outputto.write("\n")
        except Exception:
            log.critical('fwrite failed', exc_info=True)
            return False

        outputto.write("</div>")
        outputto.write("\n")

    outputto.write("</div>")
    outputto.write("\n")
    outputto.close()

    # write the final html file
    indexhead= """<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="main.css">
      <title>clogistics</title>
    </head>
    <body>
    <div id="header"> <div class"text"> <p> In the global pandemic of our time this research trajectory was conditioned to conduct on a slower paste. One day a month, a morning and or afternoon with time to think and work outside the assembly.  A shift in everyday dynamic called for a retrospect on our digital and technological selfs.
 </p> In mind there where six session planned to be stretched over a year with at the end a final substitute of our time together. One year and two months later the trajectory demanded an overflow of impressions, thinking, capacity and participation forcing to assemble the material and ourselves in almost the double amount of time. Working in different atmospheres, online, offline, high- and down-time, makes the encounter in this publication one to dig through rather than to read along. Two destinations in different webs: one on the surface and the other in the deep. <p> </div> 
    </div>      
    <div class="column>  <h1> Flushed from the deep web </h1>
     <div id="flush_container"> </div> 
     </div>
    <div class="column">
    <h1> CLOG INBOX </h1>

      <div class="container">"""
    indexfoot= """
      </div>
      </div>
      </body>
      </html>"""

    final = open("/var/www/clog/index.html", "w+")
    final.write(indexhead)

    with open("/var/www/clog/snippets.txt", "r") as inj:
        for line in inj:
            final.write(line)
        inj.close()

    final.write(indexfoot)
    final.close()

    print("done")

    return True

def testpub():
    print('put stuff to test here')
    msg = {
            "subject": "test",
            "body": "lalala body",
            "date": datetime.datetime.now(),
            "attachments": [
                {
                    "description": "lala",
                    "mimetype": "image/jpeg",
                    "data": b'asdfasdfasdf',
                    "extension": "jpg"
                },
                {
                    "description": "lala23",
                    "mimetype": "image/png",
                    "data": b'asdfasdfasdf',
                    "extension": "jpg"
                }]
            }

    msg2 = {
            "subject": "test",
            "body": "lalala body",
            "date": datetime.datetime.now(),
            "attachments": [ ]
            }


    publish(msg)
    publish(msg2)
    publish(msg)



if __name__ == '__main__':
    testpub()
