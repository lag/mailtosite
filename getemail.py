#!/usr/bin/env python3
import html
import mailparser
import sys
import traceback
import logging
from logging.handlers import RotatingFileHandler
import configparser
import email
from time import sleep
from datetime import datetime, time
from imapclient import IMAPClient
import pprint
import publishmsg as pub
import base64
import email

#for testing
USE_FLAGS = True

pp = pprint.PrettyPrinter(depth=6)
# Setup the log handlers to stdout and file.
log = logging.getLogger('imap_monitor')
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s | %(name)s | %(levelname)s | %(message)s'
    )
handler_stdout = logging.StreamHandler(sys.stdout)
handler_stdout.setLevel(logging.DEBUG)
handler_stdout.setFormatter(formatter)
log.addHandler(handler_stdout)
if False:
    handler_file = RotatingFileHandler(
        'imap_monitor.log',
        mode='a',
        maxBytes=1048576,
        backupCount=9,
        encoding='UTF-8',
        delay=True
        )
    handler_file.setLevel(logging.DEBUG)
    handler_file.setFormatter(formatter)
    log.addHandler(handler_file)

def fetchmail(imap, selector='ALL'):
    try:
        #result = imap.search('UNSEEN')
        result = imap.search(selector)
    except Exception:
        log.exception('failed search ' + str(e))
    log.info('{0} {2} messages seen - {1}'.format(len(result), result, selector))

    #for num in result:
    #    msgid = imap.fetch(num, "RFC822.HEADER")
    #            #'(BODY.PEEK[HEADER])')
    #    log.info("fetched: " + str(msgid))


    for msgid in result:
        for uid, message_data in imap.fetch(msgid, ["FLAGS" ,"RFC822"]).items():
            try:
                email_bytes = message_data[b"RFC822"]
            except:
                log.error("error in msg: " + str(uid), exc_info=True )
                pp.pprint(message_data)
                continue

            flags = message_data[b"FLAGS"]
            ##FIXME do not download all mail, just the ones with the flags we need
            if b"PUBLISHED" in flags and USE_FLAGS:
                log.info(str(msgid) + " is already published, skipping")
                continue
            if b"PUBLISHING_FAILED" in flags and USE_FLAGS:
                log.error(str(msgid) + " publishing failed before, skipping")
                continue


            #print("flags: " + str(flags))
            parsed_mail = mailparser.parse_from_bytes(email_bytes)
            parsed_email = email.message_from_bytes(email_bytes)
            print(parsed_email.get('Message-ID'))
            #pp.pprint(parsed_mail.attachments)
            #{'binary': True,
            #'charset': None,
            #'content-disposition': 'inline;\r\n filename="NtFKVL0.png"',
            #'content-id': '<part1.4E46E526.062C2B94@gmail.com>',
            #'content_transfer_encoding': 'base64',
            #'filename': 'NtFKVL0.png',
            #'mail_content_type': 'image/png',
            #'payload': 'iVBORw0KGgoA
            puba = []
            allowed_mime = {
                    'image/png': 'png',
                    'image/jpeg': 'jpg',
                    'image/gif': 'gif'
                    }
            for a in parsed_mail.attachments:
                if a['binary'] == True and a['content_transfer_encoding'] == 'base64' and a['mail_content_type'] in allowed_mime:
                    log.info('binary  ' + a['filename'] + ' mime: ' + a['mail_content_type'] + ' newext: ' + allowed_mime[a['mail_content_type']])
                    atta = {}
                    atta['mimetype'] = a['mail_content_type']
                    atta['extension'] = allowed_mime[a['mail_content_type']]
                    atta['description'] = html.escape(a['filename'])
                    try:
                        atta['data'] = base64.b64decode(a['payload'])
                    except e:
                        log.info('decode failed, skipping')
                        continue
                    puba.append(atta)
                else:
                    log.critical('attachment wrong', str(a))
                     
            reply = {
                    "type": "email",
                    "id": parsed_email.get('Message-ID'),
                    "to": parsed_mail.from_
                    }

            log.info('msgid: ' + str(msgid) + ' uid: '+ str(uid) + " flags: " + str(flags) + " from: " + str(parsed_mail.from_) + " subject " + str(parsed_mail.subject) + str(reply) )
            pubmsg = {
                    "from": str(parsed_mail.from_),
                    "body": html.escape(parsed_mail.text_plain[0]),
                    "subject": html.escape(parsed_mail.subject),
                    "date": parsed_mail.date,
                    "attachments": puba
                    }

            if pub.publish(pubmsg):
                if reply['id'] != '':
                    log.info("publishing success")
                    imap.set_flags(msgid, b'PUBLISHED')
                else:
                    imap.set_flags(msgid, b'PUBLISHING_FAILED')
                    log.critical("publishing failed")

                    #imap.add_flags('STORE', [ str(reply['id']) ], b'\UNSEEN')
                #imap.add_flags('STORE', [ reply['id'] ], b'\UNREAD')



def main():
    log.info('... script started')
    while True:
    # <--- Start of configuration section

    # Read config file - halt script on failure
        try:
            config_file = open('imap.ini','r+')
        except IOError:
            log.critical('configuration file is missing')
            break
        config = configparser.ConfigParser()
        config.readfp(config_file)

        # Retrieve IMAP host - halt script if section 'imap' or value
        # missing
        try:
            host = config.get('imap', 'host')
        except ConfigParser.NoSectionError:
            log.critical('no "imap" section in configuration file')
            break
        except ConfigParser.NoOptionError:
            log.critical('no IMAP host specified in configuration file')
            break

        # Retrieve IMAP username - halt script if missing
        try:
            username = config.get('imap', 'username')
        except ConfigParser.NoOptionError:
            log.critical('no IMAP username specified in configuration file')
            break

        # Retrieve IMAP password - halt script if missing
        try:
            password = config.get('imap', 'password')
        except ConfigParser.NoOptionError:
            log.critical('no IMAP password specified in configuration file')
            break

        # Retrieve IMAP SSL setting - warn if missing, halt if not boolean
        try:
            ssl = config.getboolean('imap', 'ssl')
        except ConfigParser.NoOptionError:
            # Default SSL setting to False if missing
            log.warning('no IMAP SSL setting specified in configuration file')
            ssl = False
        except ValueError:
            log.critical('IMAP SSL setting invalid - not boolean')
            break

        # Retrieve IMAP folder to monitor - warn if missing
        try:
            folder = config.get('imap', 'folder')
        except ConfigParser.NoOptionError:
            # Default folder to monitor to 'INBOX' if missing
            log.warning('no IMAP folder specified in configuration file')
            folder = 'INBOX'

        while True:
            # <--- Start of IMAP server connection loop

            # Attempt connection to IMAP server
            log.info('connecting to IMAP server - {0}'.format(host))
            try:
                imap = IMAPClient(host, use_uid=True, ssl=ssl)
            except Exception:
                # If connection attempt to IMAP server fails, retry
                etype, evalue = sys.exc_info()[:2]
                estr = traceback.format_exception_only(etype, evalue)
                logstr = 'failed to connect to IMAP server - '
                for each in estr:
                    logstr += '{0}; '.format(each.strip('\n'))
                log.error(logstr)
                sleep(10)
                continue
            log.info('server connection established')

            # Attempt login to IMAP server
            log.info('logging in to IMAP server - {0}'.format(username))
            try:
                result = imap.login(username, password)
                log.info('login successful - {0}'.format(result))
            except Exception:
                # Halt script when login fails
                etype, evalue = sys.exc_info()[:2]
                estr = traceback.format_exception_only(etype, evalue)
                logstr = 'failed to login to IMAP server - '
                for each in estr:
                    logstr += '{0}; '.format(each.strip('\n'))
                log.critical(logstr)
                break

            # Select IMAP folder to monitor
            log.info('selecting IMAP folder - {0}'.format(folder))
            try:
                result = imap.select_folder(folder)
                log.info('folder selected')
            except Exception:
                # Halt script when folder selection fails
                etype, evalue = sys.exc_info()[:2]
                estr = traceback.format_exception_only(etype, evalue)
                logstr = 'failed to select IMAP folder - '
                for each in estr:
                    logstr += '{0}; '.format(each.strip('\n'))
                log.critical(logstr)
                break

            fetchmail(imap, 'ALL')
            while True:
                # <--- Start of mail monitoring loop

                # After all unread emails are cleared on initial login, start
                # monitoring the folder for new email arrivals and process
                # accordingly. Use the IDLE check combined with occassional NOOP
                # to refresh. Should errors occur in this loop (due to loss of
                # connection), return control to IMAP server connection loop to
                # attempt restablishing connection instead of halting script.
                imap.idle()
                # TODO: Remove hard-coded IDLE timeout; place in config file
                result = imap.idle_check(30)
                if result:
                    imap.idle_done()
                ##CHECKING
                    fetchmail(imap, 'UNSEEN')
                else:
                    imap.idle_done()
                    imap.noop()
                    log.info('no new messages seen')
                # End of mail monitoring loop --->

            # End of IMAP server connection loop --->
            break

        # End of configuration section --->
        break
    log.info('script stopped ...')

if __name__ == '__main__':
    main()
