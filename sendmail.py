#!/usr/bin/env python3
import html
#import mailparser
import sys
import traceback
import logging
from logging.handlers import RotatingFileHandler
import configparser
import email
from time import sleep
from datetime import datetime, time
#from imapclient import IMAPClient
import pprint
#import publishmsg as pub
import base64
import email
import magic
mime = magic.Magic(mime=True)
import smtplib
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

class mailsender:
    def __init__(self):

        # Read config file - halt script on failure
        try:
            config_file = open('imap.ini','r+')
        except IOError:
            log.critical('configuration file is missing')
            exit(1)
        config = configparser.ConfigParser()
        config.readfp(config_file)
        
        # Retrieve IMAP host - halt script if section 'imap' or value 
        # missing
        try:
            self.host = config.get('imap', 'host')
        except ConfigParser.NoSectionError:
            log.critical('no "imap" section in configuration file')
            exit(1)
        except ConfigParser.NoOptionError:
            log.critical('no IMAP host specified in configuration file')
            exit(1)
        
        # Retrieve IMAP username - halt script if missing
        try:
            self.username = config.get('imap', 'username')
        except ConfigParser.NoOptionError:
            log.critical('no IMAP username specified in configuration file')
            exit(1)
        
        # Retrieve IMAP password - halt script if missing
        try:
            self.password = config.get('imap', 'password')
        except ConfigParser.NoOptionError:
            log.critical('no IMAP password specified in configuration file')
            exit(1)

    def send_file(self, file):
        files = list()
        files.append(file)
        self.send_mail('clogmailsite@riseup.net', 'clogmailsite@riseup.net', 'new file for clog', 'there is no message', files=files)

    def send_text(self, text):
        self.send_mail('clogmailsite@riseup.net', 'clogmailsite@riseup.net', 'new text for clog', text)


    def send_mail(self, send_from, send_to, subject, message, files=[]):

        server = self.host
        port = 587
        username = self.username
        password = self.password
        use_tls=True

        msg = MIMEMultipart()
        msg['From'] = send_from
        msg['To'] = COMMASPACE.join(send_to)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject

        msg.attach(MIMEText(message))

        for path in files:
            mime = magic.Magic(mime=True)
            mimetype = mime.from_file(path).split('/')
            print('mimetype is: ',str(mimetype[0]), str(mimetype[1]))
            part = MIMEBase(mimetype[0], mimetype[1])
            with open(path, 'rb') as file:
                part.set_payload(file.read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition',
                            'attachment; filename={}'.format(Path(path).name))
            msg.attach(part)

        print('sending: ', subject, message ,str(files))
        smtp = smtplib.SMTP(server, port)
        if use_tls:
            smtp.starttls()
        smtp.login(username, password)
        smtp.sendmail(send_from, send_to, msg.as_string())
        smtp.quit()
        print('end sending')

if __name__ == '__main__':
    print('main, looping')

    while True:
     
        sender = mailsender()
        sender.send_mail('clogmailsite@riseup.net', 'clogmailsite@riseup.net', 'new mail for clog', 'the message')
