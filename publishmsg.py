#!/usr/bin/env python3
#import eventlet
#imapclient = eventlet.import_patched('imapclient')

import mailparser
from datetime import datetime, time
import pprint
pp = pprint.PrettyPrinter(depth=6)
import datetime
import uuid
import logging
import sys
import traceback
import logging
import configparser
from logging.handlers import RotatingFileHandler
#

pp = pprint.PrettyPrinter(depth=6)
# Setup the log handlers to stdout and file.
log = logging.getLogger('web_publisher')
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s | %(name)s | %(levelname)s | %(message)s'
    )
handler_stdout = logging.StreamHandler(sys.stdout)
handler_stdout.setLevel(logging.DEBUG)
handler_stdout.setFormatter(formatter)
log.addHandler(handler_stdout)

try:
    config_file = open('web.ini','r+')
except IOError:
    log.critical(' web.ini configuration file is missing')
    exit()
config = configparser.ConfigParser()
config.readfp(config_file)

try:
    imagepath = config.get('web', 'imagepath')
except ConfigParser.NoSectionError:
    log.critical('no "web" section in configuration file')
    exit()
except ConfigParser.NoOptionError:
    log.critical('no imagepath specified in configuration file')


def publish(msg):
    log.info('publishing message: ' + str(msg['date']) + ' ' + str(msg['subject']))
 
    for a in msg['attachments']:
        #print(a['mimetype'] + ' ' + a['description'])
        filename = str(uuid.uuid4()) + '.' + a['extension']
        try:
            f = open("pics/" + filename, 'wb')
        except:
            log.critical('fopen failed!', exc_info=True) 
            
            return False

        try:
            f.write(a['data'])
        except Exception:
            log.critical('fwrite failed', exc_info=True)
            return False
    
    return True

def testpub():
    print('put stuff to test here')
    msg = {
            "subject": "test",
            "body": "lalala body",
            "date": datetime.datetime.now(),
            "attachments": [ 
                {
                    "description": "lala",
                    "mimetype": "image/jpeg",
                    "data": b'asdfasdfasdf',
                    "extension": "jpg"
                },
                {
                    "description": "lala23",
                    "mimetype": "image/png",
                    "data": b'asdfasdfasdf',
                    "extension": "jpg"
                }]
            }
   
    msg2 = {
            "subject": "test",
            "body": "lalala body",
            "date": datetime.datetime.now(),
            "attachments": [ ]
            }


    publish(msg)
    publish(msg2)
    publish(msg)



if __name__ == '__main__':
    testpub()
